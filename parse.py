import re

from utils import error_message

TOKEN_INDENT = 'indent'
TOKEN_OUTDENT = 'outdent'
TOKEN_VALUE = 'value'
TOKEN_END = 'end'


class ParseError(Exception):
    pass


def line_tokens(source):
    indents = []
    current = 0
    for line in source.strip().split('\n'):
        for spaces, rest in re.findall('(\s*)(\S.*)', line):
            indent = len(spaces)
            if indent > current:
                indents.append(current)
                current = indent
                yield TOKEN_INDENT, ''
            elif indent < current:
                yield TOKEN_OUTDENT, ''
                while True:
                    current = indents.pop()
                    if indent == current:
                        break
                    if indent > current:
                        raise ParseError(indent, current, rest)
                    yield TOKEN_OUTDENT, ''
            yield TOKEN_VALUE, rest
    while indents:
        yield TOKEN_OUTDENT, ''
        indents.pop()
    yield TOKEN_END, ''


def identifier_rest(value):
    identifier = ''
    rest = ''
    for index, char in enumerate(value):
        if char.isalnum() or char in ['_', '-']:
            identifier += char
        else:
            rest = value[index:].lstrip()
            break
    if not identifier:
        raise ParseError('invalid col line', value)
    return identifier, rest


class Parser(object):
    """
    Current sample source:
        template ^$ home.html
            row
                col3 Log In
                col5
                    image teamphoto.jpg
                col4
                    text This is a message
        template ^login/$ login.html
            row
                col2
                    text User Id
                col4
                    text johndoe
            row
                col2
                    text Password
                col4
                    text johndoe
            row
                col6 Log In
    With output:
        url: ^$    filename: home.html    html:
        {% extends "base.html" %}
        {% block content %}
        <div class="container">
          <div class="row">
            <div class="col-md-3">
              <button class="btn">Log In</button>
            </div>
            <div class="col-md-5">
              <img src="teamphoto.jpg"></img>
            </div>
            <div class="col-md-4">
              <pre>This is a message</pre>
            </div>
          </div>
        </div>
        {% endblock %}

        url: ^login/$    filename: login.html    html:
        {% extends "base.html" %}
        {% block content %}
        <div class="container">
          <div class="row">
            <div class="col-md-2">
              <pre>User Id</pre>
            </div>
            <div class="col-md-4">
              <pre>johndoe</pre>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <pre>Password</pre>
            </div>
            <div class="col-md-4">
              <pre>johndoe</pre>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <button class="btn">Log In</button>
            </div>
          </div>
        </div>
        {% endblock %}
    """

    def __init__(self, filename):
        self.filename = filename

    def parse(self):
        templates = self.build_templates()
        if templates is None:
            return

        def addline(line):
            self.output += line + '\n'

        for template in templates:
            self.output = ''
            addline('{% extends "base.html" %}')
            addline('{% block content %}')
            addline('<div class="container">')
            for row in template['rows']:
                addline('  <div class="row">')
                for coltype, objtype, value in row:
                    colclass = 'col-md-' + coltype[3:]
                    addline('    <div class="%s">' % colclass)
                    if objtype == 'button':
                        addline('      <button class="btn">%s</button>' % value)
                    elif objtype == 'text':
                        addline('      <pre>%s</pre>' % value)
                    elif objtype == 'image':
                        addline('      <img src="%s"></img>' % value)
                    else:
                        return error('internal error - invalid objtype', objtype)
                    addline('    </div>')
                addline('  </div>')
            addline('</div>')
            addline('{% endblock %}')
            template['html'] = self.output

        return templates

    def build_templates(self):
        templates = []
        level = 'template'
        current_template = {}
        current_col = []
        coltype = ''

        def addcol(col):
            current_template['rows'][-1].append(col)

        try:
            tokens = list(line_tokens(open(self.filename).read()))
        except ParseError:
            return
        for token, value in tokens:
            if token == TOKEN_INDENT:
                if level == 'template':
                    level = 'row'
                elif level == 'row':
                    level = 'col'
                else:
                    level = 'subcol'
            elif token == TOKEN_OUTDENT:
                if level == 'subcol':
                    level = 'col'
                elif level == 'col':
                    level = 'row'
                else:
                    level = 'template'
            elif token == TOKEN_VALUE:
                if level == 'template':
                    if current_template:
                        if current_col:
                            addcol(current_col)
                            current_col = []
                        templates.append(current_template)
                    parts = value.split()
                    if len(parts) != 3:
                        return error('invalid template line', value)
                    current_template = dict()
                    current_template['url'] = parts[1]
                    current_template['filename'] =  parts[2]
                    current_template['rows'] = []
                elif level == 'row':
                    if current_col:
                        addcol(current_col)
                    current_template['rows'].append([])
                    current_col = []
                elif level == 'col':
                    if current_col:
                        addcol(current_col)
                    try:
                        coltype, rest = identifier_rest(value)
                    except ParseError:
                        return error('invalid col line', value)
                    if rest:
                        current_col = [coltype, 'button', rest]
                    else:
                        current_col = []
                else:
                    try:
                        objtype, rest = identifier_rest(value)
                    except ParseError:
                        return error('invalid subcol line', value)
                    current_col = [coltype, objtype, rest]
            elif token == TOKEN_END:
                if current_col:
                    addcol(current_col)
                if current_template:
                    templates.append(current_template)
                break
            else:
                return error('internal error - invalid token', token)

        return templates

