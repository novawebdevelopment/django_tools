#!/usr/bin/python

import os
import sys
import glob

import parse
import patch
from utils import error_message, run_command

rootdir = os.path.dirname(os.path.realpath(__file__))

valid_commands = {
    'help': 'print help',
    'start': 'create git repo, starting django project, optionally app as well',
    'clone': 'clones github repo, creates settings_dev.py, syncs and starts',
    'dumpdata': 'create json.dump and media.tar in temp dir',
    'loaddata': 'load database from json.dump and media.tar in temp dir',
    'copydata': 'scp json.dump and media.tar from temp dir to remote temp dir',
    'rs': 'ensure venv is activated, then runserver',
    'sd': 'ensure venv is activated, then syncdb (optional database backup and reset)',
    'genhtml': 'generate html templates from source code',
}


class Validator(object):
    def __init__(self):
        self.positional_args = []
        self.options = {}
        self.valid = self.internal_checks()
        if self.valid:
            self.valid = self.parse_validate_argv()

    def internal_checks(self):
        executor_commands  = [attr[4:] for attr in dir(Executor) if attr.startswith('cmd_')]
        for command in executor_commands:
            if command not in valid_commands:
                return error_message('internal error - Executor has an invalid command', 'cmd_' + command)
        for command in valid_commands:
            if command not in executor_commands:
                return error_message('internal error - Validator has an invalid command', command)
        return True

    def parse_validate_argv(self):
        for arg in sys.argv[1:]:
            if arg.startswith('--'):
                parts = arg[2:].split('=')
                if len(parts) < 1 or len(parts) > 2 or not parts[0] or arg.endswith('='):
                    return error_message('invalid command line option', arg)
                value = ''
                if len(parts) > 1:
                    value = parts[1]
                self.options[parts[0]] = value
            else:
                self.positional_args.append(arg)
        if not self.positional_args:
            self.command = 'help'
        else:
            self.command = self.positional_args.pop(0)
        if self.command not in valid_commands:
            return error_message('invalid command', self.command)
        return True


class Executor(object):
    def __init__(self, validator):
        self.validator = validator

    def execute(self):
        if not self.validator.valid:
            return
        if self.validator.command == 'start':
            if not self.validator.positional_args:
                return error_message('start command needs at least a project name')
            self.dirname = self.validator.positional_args[0]
            self.dircmd = 'cd %s && ' % self.dirname
            if len(self.validator.positional_args) > 1:
                self.sitename = self.validator.positional_args[1]
            else:
                self.sitename = self.dirname
            if len(self.validator.positional_args) > 2:
                self.appname = self.validator.positional_args[2]
            else:
                self.appname = ''
        elif self.validator.command != 'help':
            if os.system('git status > /dev/null') or not glob.glob('venv'):
                return error_message('must be in a git repo with virtual environment')
            self.dirname = '.'
            self.dircmd = ''
            self.sitename = None
            for line in open('manage.py').readlines():
                if 'DJANGO_SETTINGS_MODULE' in line:
                    line = line[:line.find('.settings')]
                    self.sitename = line[line.rfind('"') + 1:]
            self.appname = ''
        func = getattr(self, 'cmd_' + self.validator.command)
        func()

    def local_command(self, cmd):
        run_command('%s%s' % (self.dircmd, cmd))

    def venv_command(self, cmd):
        run_command('%ssource venv/bin/activate && %s' % (self.dircmd, cmd))

    def git_commit(self, message):
        run_command('%sgit add . && git commit -m "%s"' % (self.dircmd, message))

    def cmd_help(self):
        for command in sorted(valid_commands):
            print '%-20s%s' % (command, valid_commands[command])

    def cmd_rs(self):
        self.venv_command('python manage.py runserver')

    def cmd_sd(self):
        if 'reset' in self.validator.options:
            if 'force' not in self.validator.options:
                backup = 'cp database ../backup_database && rm database'
                cmd = ('source venv/bin/activate && python manage.py ' +
                       'syncdb < %s/syncdb_input > /dev/null' % rootdir)
                msg = ('The following commands will be run:\n' +
                        '    %s\n' % backup +
                        '    %s\n' % cmd +
                        'You can use --force to avoid this question.\n' +
                        'Are you sure? ("yes" or enter/"no"): ')
                var = raw_input(msg)
                while var and var not in ['yes', 'no']:
                    var = raw_input('Please enter "yes" or nothing to proceed or "no" to cancel: ')
                if var == 'no':
                    return
            run_command(backup)
            run_command(cmd)
        else:
            self.venv_command('python manage.py syncdb')

    def cmd_start(self):
        if glob.glob(self.dirname):
            if 'force' not in self.validator.options:
                return error_message('directory already exists; use --force to override', self.dirname)
            else:
                run_command('rm -rf ' + self.dirname)
        run_command('mkdir ' + self.dirname)
        self.local_command('git init')
        run_command('cp %s/requirements.txt %s' % (rootdir, self.dirname))
        run_command('cp %s/gitignore %s/.gitignore' % (rootdir, self.dirname))
        self.git_commit('initial commit')

        self.local_command('virtualenv-2.7  --distribute venv')
        self.venv_command('pip install -r requirements.txt')
        self.venv_command('django-admin.py startproject %s .' % self.sitename)
        self.git_commit('django startproject')

        if self.appname:
            self.venv_command('python manage.py startapp %s' % self.appname)
            self.git_commit('django startapp')

        patch.PatchSettings(self)()
        patch.PatchUrls(self)()
        patch.CreateViews(self)()
        if self.appname:
            patch.CreateAdmin(self)()
        self.local_command('mkdir %s/templates' % self.sitename)
        self.local_command('mkdir %s/static' % self.sitename)
        self.local_command('mkdir %s/static/css' % self.sitename)
        self.local_command('cp %s/base.html %s/templates' % (rootdir, self.sitename))
        self.local_command('cp %s/home.html %s/templates' % (rootdir, self.sitename))
        self.local_command('cp %s/global.css %s/static/css' % (rootdir, self.sitename))
        self.git_commit('development settings, enabled admin, created home page')

        if self.validator.options.get('remote'):
            self.local_command('git remote add origin %s' % self.validator.options['remote'])
            self.local_command('git push -f -u origin master')

        self.local_command('touch %s/settings.py' % self.sitename)
        self.venv_command('python manage.py syncdb < %s/syncdb_input > /dev/null' % rootdir)
        self.venv_command('python manage.py runserver')

    def cmd_clone(self):
        print 'coming soon'

    def cmd_dumpdata(self):
        tempdir = '%s/temp' % rootdir
        if not (glob.glob(tempdir)):
            run_command('mkdir %s' % tempdir)
        self.venv_command('python manage.py dumpdata --indent=4 > %s/dump.json' % tempdir)
        if (glob.glob('%s/media' % self.sitename)):
            run_command('tar czvf %s/media.tar %s/media' % (tempdir, self.sitename))
        else:
            if (glob.glob('%s/media.tar' % tempdir)):
                run_command('rm %s/media.tar' % tempdir)

    def cmd_loaddata(self):
        tempdir = '%s/temp' % rootdir
        self.venv_command('python manage.py loaddata %s/dump.json' % tempdir)
        if (glob.glob('%s/media.tar' % tempdir)):
            run_command('tar xzvf %s/media.tar %s/media' % (tempdir, self.sitename))

    def cmd_copydata(self):
        print 'coming soon'

    def cmd_genhtml(self):
        if not self.validator.positional_args:
            return error_message('genhtml command needs a source file name')
        self.templates = parse.Parser(self.validator.positional_args[0]).parse()
        if self.templates is None:
            return
        for template in self.templates:
            print 'url: %s    filename: %s    html:' % (template['url'], template['filename'])
            print template['html']
        patch.PatchUrlsFromParser(self)()



if __name__ == '__main__':
    Executor(Validator()).execute()

