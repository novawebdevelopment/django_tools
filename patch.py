"PatchFile class built for Executor so far; Parser to use it soon."


class PatchFile(object):
    def __init__(self, executor):
        self.executor = executor
        self.dirname = executor.dirname
        self.sitename = executor.sitename
        self.appname = executor.appname

    @property
    def lines(self):
        return [line.rstrip() for line in open(self.get_filename()).readlines()]

    def get_filename(self):
        raise NotImplementedError

    def patch(self):
        raise NotImplementedError

    def __call__(self):
        self.output = []
        self.patch()
        filename = self.get_filename()
        self.file = open(filename, 'w')
        for line in self.output:
            self.file.write("%s\n" % line)
        self.file.close()


production_environment = """
# Import the production settings if environment variable set
if os.environ.get('DJANGO_ENVIRONMENT') == 'production':
    exec('from settings_production import *')
"""


class PatchSettings(PatchFile):
    def get_filename(self):
        return '%s/%s/settings.py' % (self.dirname, self.sitename)

    def patch(self):
        inside_staticfiles = False
        inside_templatedirs = False
        inside_installedapps = False
        self.output.extend([
            'import os',
            'SITE_ROOT = os.path.dirname(os.path.realpath(__file__))',
            '',
        ])

        for index, line in enumerate(self.lines):
            if 'django.db.backends.' in line:
                line = line.replace('backends.', 'backends.sqlite3')
            if 'path to database file if using sqlite3' in line:
                line = line.replace("''", "'database'")

            if line == 'STATICFILES_DIRS = (':
                inside_staticfiles = True
            elif inside_staticfiles and line == ')':
                self.output.append("    os.path.join(SITE_ROOT, 'static'),")
                inside_staticfiles = False

            if line == 'TEMPLATE_DIRS = (':
                inside_templatedirs = True
            elif inside_templatedirs and line == ')':
                self.output.append("    os.path.join(SITE_ROOT, 'templates'),")
                inside_templatedirs = False

            if line == 'INSTALLED_APPS = (':
                inside_installedapps = True
            elif inside_installedapps and line == ')':
                if self.appname:
                    self.output.append("    '%s'," % self.appname)
                inside_installedapps = False

            if 'MEDIA_URL =' in line:
                line = "MEDIA_URL = '/media/'"
            if 'MEDIA_ROOT =' in line:
                line = "MEDIA_ROOT = os.path.join(SITE_ROOT, 'media')"

            if "'django.contrib.admin'" in line:
                line = line.replace('# ', '')
            self.output.append(line)

        for line in production_environment.split('\n'):
            self.output.append(line.rstrip())


media_urls = """
if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )
"""


class PatchUrls(PatchFile):
    def get_filename(self):
        return '%s/%s/urls.py' % (self.dirname, self.sitename)

    def patch(self):
        skip = False
        for line in self.lines:
            if skip:
                skip = False
                continue
            if ('Uncomment the next two lines' in line or
                'from django.contrib import admin' in line or
                'admin.autodiscover()' in line):
                if 'admin.autodiscover()' in line:
                    skip = True
                continue
            if 'include(admin.site.urls))' in line:
                line = line.replace('# ', '')
            self.output.append(line)
            if 'from django.conf.urls import patterns' in line:
                self.output.append('from django.contrib import admin')
                self.output.append('from django.conf import settings')
                self.output.append('')
                self.output.append('import views')
                self.output.append('')
                self.output.append('admin.autodiscover()')
            if '^%s/' % self.sitename in line:
                self.output.append("    url(r'^$', views.HomeView.as_view()),")

        for line in media_urls.split('\n'):
            self.output.append(line.rstrip())


class CreateViews(PatchFile):
    def get_filename(self):
        return '%s/%s/views.py' % (self.dirname, self.sitename)

    def patch(self):
        self.output.append('from django.views import generic\n')
        self.output.append('class HomeView(generic.TemplateView):')
        self.output.append("    template_name = 'home.html'")


register_models = """
for item in [getattr(models, attr) for attr in dir(models)]:
    if 'django.db.models.base.ModelBase' in str(item.__class__):
        admin.site.register(item)
"""


class CreateAdmin(PatchFile):
    def get_filename(self):
        return '%s/%s/admin.py' % (self.dirname, self.appname)

    def patch(self):
        self.output.append('from django.contrib import admin\n')
        self.output.append('from %s import models\n' % self.appname)
        for line in register_models.split('\n'):
            self.output.append('%s' % line.rstrip())


class PatchUrlsFromParser(PatchFile):
    def get_filename(self):
        return '%s/%s/urls.py' % (self.dirname, self.sitename)

    def patch(self):
        need_import = True
        inside_imports = False
        inside_urls = False
        urls = []
        for line in self.lines:
            if 'django' in line and 'views' in line and 'generic' in line:
                need_import = False
            if need_import:
                if 'from django' in line:
                    inside_imports = True
                if inside_imports and not line:
                    self.output.append('from django.views import generic')
                    need_import = False
            if '# url' in line and 'home' in line:
                inside_urls = True
            if inside_urls and not line:
                inside_urls = False
                for template in self.executor.templates:
                    if template['url'] in urls:
                        continue
                    self.output.append(self.build_url(template))
            for template in self.executor.templates:
                if template['url'] in line and '# url' not in line:
                    line = self.build_url(template)
                    urls.append(template['url'])
                    break
            self.output.append(line)

    def build_url(self, template):
        fmt = "    url(r'%s', generic.TemplateView.as_view(template_name='%s'),"
        return fmt % (template['url'], template['filename'])

