import os
import sys


def error_message(message, *args):
    print 'ERROR: %s' % message
    for arg in args:
        print '    ' + arg


def run_command(cmd):
    print cmd
    rv = os.system(cmd)
    if rv:
        print 'rv: %s' % rv
        sys.exit()

